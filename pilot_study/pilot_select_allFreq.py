#! /usr/bin/env python

import sys, re


def get_args(cline = sys.argv[1:]):

    '''
    get argument in command line
    '''
    
    import argparse
    parser = argparse.ArgumentParser(description = '''Get information for 3 pilot individuals ''')
    parser.add_argument('input_file', help = 'list of SNP in protein involved in phosphorylation network')
    args = parser.parse_args(cline)
    return args


def selection(input_file):
        
    all_ind = open(input_file, 'r')
    ind_1 = open("/home/yfourne/chicago_test/pilot_study/all_freq/pilot_18486_allFreq", 'a')
    ind_2 = open("/home/yfourne/chicago_test/pilot_study/all_freq/pilot_18862_allFreq", 'a')
    ind_3 = open("/home/yfourne/chicago_test/pilot_study/all_freq/pilot_19160_allFreq", 'a')

    while True:    
        line = all_ind.readline()
        if not line:
            break

        info = line.split('\t')

        try:
            freq = float(info[6])
        except:
            freq = '-'
        
        try:
            AAi = re.split(r'(\d+)', info[12])
            Ar = AAi[0]
            pos = AAi[1]
            Aa = AAi[2]
        except:
            pos = '-'
            Ar = '-'
            Aa = '-'
        
        try:
        	prt_id = info[18+121].split('\n')[0]
        except:
        	prt_id = '-'

        if freq == '-' or freq < 0.05:
        	freq_5 = '-'
        else:
        	freq_5 = 'yes'


        if info[18+1] != info[18+108]:
        	if info[18+1] != info[18+48]:

        		if info[18+1] == '1|1':
        			ind_1.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '18862-19160', '-', pos, Ar, Aa]) + '\n')
        		elif info[18+1] == '1|0' or info[18+1] == '0|1':
        			ind_1.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '18862-19160', '-', pos, Ar, Aa]) + '\n')

        		if info[18+108] != info[18+48]:

        			if info[18+108] == '1|1':
        				ind_2.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '18486-19160', '-', pos, Ar, Aa]) + '\n')
        			elif info[18+108] == '1|0' or info[18+108] == '0|1':
        				ind_2.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '18486-19160', '-', pos, Ar, Aa]) + '\n')

        			if info[18+48] == '1|1':
        				ind_3.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '18486-18862', '-', pos, Ar, Aa]) + '\n')
        			elif info[18+48] == '1|0' or info[18+48] == '0|1':
        				ind_3.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '18486-18862', '-', pos, Ar, Aa]) + '\n')

        		else:

        			if info[18+108] == '1|1':
        				ind_2.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '18486', '19160', pos, Ar, Aa]) + '\n')
        			elif info[18+108] == '1|0' or info[18+108] == '0|1':
        				ind_2.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '18486', '19160', pos, Ar, Aa]) + '\n')

        			if info[18+48] == '1|1':
        				ind_3.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '18486', '18862', pos, Ar, Aa]) + '\n')
        			elif info[18+48] == '1|0' or info[18+48] == '0|1':
        				ind_3.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '18486', '18862', pos, Ar, Aa]) + '\n')

        	else:

        		if info[18+1] == '1|1':
        			ind_1.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '18862', '18486', pos, Ar, Aa]) + '\n')
        		elif info[18+1] == '1|0' or info[18+1] == '0|1':
        			ind_1.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '18862', '18486', pos, Ar, Aa]) + '\n')

        		if info[18+48] == '1|1':
        			ind_3.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '18862', '19160', pos, Ar, Aa]) + '\n')
        		elif info[18+48] == '1|0' or info[18+48] == '0|1':
        			ind_3.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '18862', '19160', pos, Ar, Aa]) + '\n')

        else:

        	if info[18+1] == '1|1':
        		ind_1.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '19160', '18862', pos, Ar, Aa]) + '\n')
        	elif info[18+1] == '1|0' or info[18+1] == '0|1':
        		ind_1.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '19160', '18862', pos, Ar, Aa]) + '\n')

        	if info[18+108] == '1|1':
        		ind_2.write('\t'.join([prt_id, info[2], freq_5, 'yes', 'no', info[3], info[4], info[0], info[1], '19160', '18486', pos, Ar, Aa]) + '\n')
        	elif info[18+108] == '1|0' or info[18+108] == '0|1':
        		ind_2.write('\t'.join([prt_id, info[2], freq_5, 'no', 'yes', info[3], info[4], info[0], info[1], '19160', '18486', pos, Ar, Aa]) + '\n')

#####################################################################################################

if __name__ == '__main__':
    args = get_args()
    selection(args.input_file)