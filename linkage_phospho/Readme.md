Genetic variation alters phosphorylation networks
============================================

The objective is to link genetics variations with different protein modification,
from different VCF variant file with predicting effect provided by SnpEff 
 
-------------------------------------------------

The first step is to download all dataset of proteins that we would like study.

Here, we study protein involved in the phosphorylation network

the list of all dataset that we download is in the following file:

Dataset_information.txt

-------------------------------------------------
 
As protein file come from of different web site (studies), files formats are also different.

It is more simple to have the same format for all files (without protein sequence).

We chose to have tab format for all file:
 
+ csv-tab.py: Convertion CSV file to tab format
+ fasta_file_info.py: Convertion Fasta file to tab Format (information line without sequence)
+ convert_data_set2_from_Freschi2014.py: Convertion from specific dataset from previously study
+ phophosite_in_tab.R: Conbertion from phophosite web site (space separation)

-------------------------------------------------

####Get Gene information 
To link varation with protein, we before need to identify gene encoding protein.
we could find it from unipot accession number or ensembl protein ID.
From previous file processing to tab format

+ get_gen_info.R
+ info_dataset_S2.R
+ info_phosphosite.R

> N.B.:
It is necessary to download and install BiomaRt (R package)


####Get Variation in domain
the first is to get variation to all protein for each protein
use filter.txt file result from convertion processing (all SNPs or SNPs per chromosome)

+ link.py

The second step is to get variation to phosphorylate and binding domain
use file from link processing result

+ variation_domain.py 

----------------------------------------------------

#### Shell_script folder 
Shell scripts are used to process several file  
Specify input and output folder/file in command line argument for all script 

+ fa-tab.sh: Run fasta_file_info.py
+ link_prot.sh: Run link.py
+ domain.sh: Run variation_domain.py

