Phosphorylation Data bases download



-Phospho.EML
A databases of S/T/Y phosphorylation site

*Phospho.ELM datasets

phosphoELM_vertebrate_2011-11.dump

tab-delimited format 
location: /mnt/lustre/home/y.fourne/polymorph/linkage_phospho/ELM

with Release version 9.0 september 2014
Download July 08th 2014

*Reference
"Phospho.ELM: a database of phosphorylation sites - update 2011"; 
Dinkel H, Chica C, Via A, Gould CM, Jensen LJ, Gibson TJ, Diella F. Nucleic Acids Res. 2010 Nov 9. PMID: 21062810
Phospho.ELM version 9.0 (September 2010) 
contains 8,718 substrate proteins from different species covering 3,370 tyrosine, 31,754 serine and 7,449 threonine instances.
URL: http://phospho.elm.eu.org/index.html



-Kinase site
Genomics, evolution and function of protein kinases

*Kinase Datasets

S1, S2, S3, S4, S5, S6, S7 

Tables in tab-delimited text format 
Table legends
location: /mnt/lustre/home/y.fourne/polymorph/linkage_phospho/kinase_site

Last update: November 04
Download: July 08th 2014

*Reference

The Protein Kinase Complement of the Human Genome
G Manning, DB Whyte, R Martinez, T Hunter, S Sudarsanam (2002).
Science 298:1912-1934
URL: http://kinase.com



-PhosphositePlus
Study of protein post-translational modifications

*Phosphosite Datasets

Disease-associated_sites.gz	
Kinase_Substrate_Dataset.gz	 
BioPAX:Kinase-substrate information	 
PTMVAR_download.xlsx.zip	 
Phosphorylation_site_dataset.gz	 	
Regulatory_sites.gz	

tab-delimited format 
location:/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/phosphositePlus

Last mofifified: Jun 03rd 2014
Download: July 08th 2014

Notabene:
Human kinases can have other substrates species
other kinases species can have human substrates

*Reference

Hornbeck PV, Kornhauser JM, Tkachev S, Zhang B, Skrzypek E, Murray B, Latham V, Sullivan M (2012)
Nucleic Acids Res. 40(Database issue), D261–70
PhosphoSitePlus®, www.phosphosite.org
URL: http://www.phosphosite.org/homeAction.do




-Pfam
Large collection of protein families

*Pfam Datasets

Phospho S/T binding domain and Phospho Y binding

Fasta file format 
location:/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/Pfam

Pfam 27.0 (March 2013, 14831 families)
Download: July 08th 2014

*Reference

The Pfam protein families database: 
M. Punta, P.C. Coggill, R.Y. Eberhardt, J. Mistry, J. Tate, C. Boursnell, N. Pang, K. Forslund, G. Ceric, J. Clements, A. Heger, L. Holm, E.L.L. Sonnhammer, S.R. Eddy, A. Bateman, R.D. Finn
Nucleic Acids Research (2014)  Database Issue 42:D222-D230
http://pfam.xfam.org




-Smart
Simple Molecular Architecture Research Tool

*Smart Datasets
Phospho S/T binding domain and Phospho Y binding

Fasta fileformat 
location:/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/SMART

SMART 7: recent updates to the protein domain annotation resource
2012 version 7.0

Download: July 08th 2014

*Reference

Schultz et al. (1998) Proc. Natl. Acad. Sci. USA 95, 5857-5864
Letunic et al. (2012) Nucleic Acids Res 40:, D302-D305
URL: http://smart.embl-heidelberg.de





-Hupho
The human phosphatase portal

*Hupho datasets

PhosphoClass_2014-07-11_21-49.csv 

CSV file format 
location:/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/Hupho

Download: July 08th 2014

*Reference
HuPho: the human phosphatase portalSusanna Liberti, Francesca Sacco, Alberto Calderone, Livia Perfetto, Marta Iannuccelli, Simona Panni, Elena Santonico, Anita Palma, Aurelio P. Nardozza, Luisa Castagnoli and Gianni Cesareni
URL: http://hupho.uniroma2.it


























