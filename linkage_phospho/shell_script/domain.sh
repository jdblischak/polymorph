#!/bin/bash


<<instruction 

Run variation_domain.py python program on all file in a folder
It's need 2 argument : Input folder with to process and output folder for output file  

instruction

LINK_DIR=$1
DOMAIN_DIR=$2

for filename in `ls $LINK_DIR/*`
do
    echo "/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/variation_domain.py  $filename -f $DOMAIN_DIR" | \
    qsub -l h_vmem=16g -N `basename $filename` -o $LINK_DIR -j y -cwd -V
done
