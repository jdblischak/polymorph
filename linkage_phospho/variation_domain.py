#!/usr/bin/env python

'''

Get variation involved in phosphorylation protein network
Linkage between variation file (SNPs or species difference) and gene coding protein position 

'''


import sys, os, glob, re


def get_args(cline = sys.argv[1:]):
    '''
    get input file command line and output file command line
    
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Select variation in phosphrylation domain ''')
    
    parser.add_argument('link_file', help = 'Link bed file between variation and gene coding protein')
    parser.add_argument('-f', '--folder_output', help = 'Folder where output file will be written')
    args = parser.parse_args(cline)

    return args
    
def get_var_domain(link_file, dir_path):

    file_type = {'14-3-3-homologue-smart':[2,'dom'],
                 '14-3-3-PF00244_selected_sequences':[1,'dom'],
                 'BRCT-PF00533_selected_sequences':[1,'dom'],
                 'BRCT-smart':[2,'dom'],
                 'C2-PF00168_selected_sequences':[1,'dom'],
                 'C2-smart':[2,'dom'],
                 'FF-PF01846_selected_sequences':[1,'dom'],
                 'FF-smart':[2,'dom'],
                 'FHA-PF00498_selected_sequences':[1,'dom'],
                 'FHA-smart':[2,'dom'],
                 'IRF-3-PF10401_selected_sequences':[1,'dom'],
                 'IRF-3-smart':[2,'dom'],
                 'MH2-PF03166_selected_sequences':[1,'dom'],
                 'POLO-BOX-PF00659_selected_sequences':[1,'dom'],
                 'PTB-PF08416_selected_sequences':[1,'dom'],
                 'PTB-smart':[2,'dom'],
                 'SH2-PF00017_selected_sequences':[1,'dom'],
                 'SH2-smart':[2,'dom'],
                 'WD40-PF00400_selected_sequences':[1,'dom'],
                 'WD40-smart':[2,'dom'],
                 'WW-PF00397_selected_sequences':[1,'dom'],
                 'WW-smart':[2,'dom'],
                 'Disease_human':[11,'win'],
                 'phosELM_human':[1,'win'],
                 'Phospho_human':[5,'win'],
                 'Regulation_phospho_human':[7,'win'],
                 'supplementary_data_set_2.txt':[2,'win']}

    fileName =  os.path.basename(link_file)[4:]
    
    if dir_path[-1] is '/':
        output_file = open(dir_path + fileName, 'w')
    else:
        output_file = open(dir_path + '/' + fileName, 'w')
    
    if fileName in file_type:
        spec = file_type[fileName]
        protein_file = glob.glob("/mnt/lustre/home/y.fourne/result/phosphoprotein/R_link/*/" + fileName)[0]
        variant = open(link_file, 'r')

        while True:
            Vline = variant.readline()
            if not Vline:
                break
            ref_name = Vline.split()[-1]
            V_pos = Vline.split()[7].split('|')[3]
            
            if V_pos != '':
                protein = open(protein_file, 'r')
                while True:
                    Pline = protein.readline()
                    if not Pline:
                        break

                    if re.search(ref_name, Pline):
                        
                        if spec[1] is 'dom':
                            lower_bound = int(Pline.split('\t')[spec[0]].split('-')[0])
                            upper_bound = int(Pline.split('\t')[spec[0]].split('-')[1])
                        elif spec[1] is 'win':
                            lower_bound =  int(re.split(r'(\d+)', Pline.split('\t')[spec[0]])[1]) - 7
                            upper_bound =  int(re.split(r'(\d+)', Pline.split('\t')[spec[0]])[1]) + 7
                    
                        if int(re.split(r'(\d+)', V_pos)[1]) in range(lower_bound, upper_bound):
                            newLine = Vline.split('\n')[0] + '\t' + Pline
                            output_file.write(newLine)
   
#####################################################################################################        
        
if __name__ == '__main__':
    args = get_args()
    get_var_domain(args.link_file, args.folder_output)
	
    