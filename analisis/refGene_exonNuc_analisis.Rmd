Variation between Human, chimpanzee and rhesus macaque
========================================================

We study the Variation nucleotide in the gnetic alignment between Human chimpanzee and reshus macaque.

Genome reference:

Human : hg19
<br>Chimpanzee : panTro2</br>
Reshus macaque : rheMac2

We can try to find :

+ How many total non-synonymous, synonomous, etc. Variation? Per chromosome? Distribution per gene?
+ How many NS variation change a tyrosine, serine, or threonine?

For that:

we classify variation effect into two categories : Non-Synonymous and Synonymous


- Non-Synonymous categories is composed to following effect:

CODON_CHANGE_PLUS_CODON_DELETION, CODON_CHANGE_PLUS_CODON_INSERTION, CODON_DELETION, CODON_INSERTION, EXON, EXON_DELETED, FRAME_SHIFT, NON_SYNONYMOUS_CODING, NON_SYNONYMOUS_START, SPLICE_SITE_REGION, START_GAINED, START_LOST, STOP_GAINED, STOP_LOST

- Synonymous categories is composed to following effect:

SYNONYMOUS_CODING, SYNONYMOUS_STOP, UTR_3_PRIME, UTR_5_PRIME

The dataset is composed to several transcipt for one variation, we decide to selected one transcript by variation because It is the same data for all transcript without Transcirpt ID (trid), Exon Rank (rank) and may be Amino Acid length (AA_len).

## Variations Distribution
The first observation we can make is the distribution of two categories by chromosome.
We can see Non_synonymous and Synonimous vairation distribution by Chromosome (Table 1 & 2, Figure 1 & 2) following by Non_synonymous and Synonimous variation distribution by Gene (Figure 3 and 4)

The chomosome who have the most variation is the chromosome 1 and we can have many variation by gene

```{r global-chunk-options, echo = FALSE, message=FALSE}
library(plotrix)
library(knitr)
opts_chunk$set(cache = TRUE)
library(pander)
panderOptions('table.style', 'rmarkdown')
```

```{r data-input, echo=FALSE}
file = "/Users/Sedjro/Desktop/internship/Species_file/refGene.exonNuc.filter.txt"
data <- read.delim(file, header = FALSE , skip = 1)
colnames(data) <- c('chr', 'pos', 'name', 'ref', 'alt', 'tr_ID', 'ExNum', 'TotEx',
                    'ExLen', 'IF', 'OF', 'VarPos', 'effect', 'impact', 'funclass',
                    'codon', 'AA', 'AA_len', 'gene', 'biotype', 'coding', 'trid',
                    'rank', 'panTro2', 'rheMac2')
NonSynonym <- c("CODON_CHANGE_PLUS_CODON_DELETION", 
                "CODON_CHANGE_PLUS_CODON_INSERTION", 
                "CODON_DELETION", "CODON_INSERTION", "EXON", "EXON_DELETED", 
                "FRAME_SHIFT", "NON_SYNONYMOUS_CODING", "NON_SYNONYMOUS_START", 
                "SPLICE_SITE_REGION", "START_GAINED", "START_LOST", "STOP_GAINED", 
                "STOP_LOST")

Synonym <- c("SYNONYMOUS_CODING", "SYNONYMOUS_STOP", "UTR_3_PRIME", "UTR_5_PRIME")
chr_list <- c('chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8',
              'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 
              'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22')
single_trans <- data[!duplicated(data[, c("effect", "pos", "chr")]),]

```

```{r get-distribution-by-chrom, echo=FALSE}
NSL_chr_tab <- NULL
SL_chr_tab <- NULL
for (chrm in unique(single_trans$chr)){
  NS <- 0
  S <- 0
  data_chr <- single_trans[single_trans$chr == chrm,]
  tab_chr <- as.data.frame(table(data_chr$effect))
  for (eff in tab_chr$Var1){
    sym <- FALSE
    for (SL in Synonym){
      if (eff == SL)
        sym <- TRUE
    }
    if (sym){
      S  <- S + tab_chr$Freq[tab_chr$Var1 == eff]
    }else{
      NS  <- NS + tab_chr$Freq[tab_chr$Var1 == eff]
    }    
  }
  NSL_chr_tab <- rbind(NSL_chr_tab, data.frame(chrm, NS))
  SL_chr_tab <- rbind(SL_chr_tab, data.frame(chrm, S))
}
NSL_chr <- NSL_chr_tab[match(chr_list, NSL_chr_tab$chrm),]
SL_chr <- SL_chr_tab[match(chr_list, SL_chr_tab$chrm),]
```

```{r get-allele-frequency-by-categories, echo=FALSE}
NS_list <- NULL
S_list <- NULL
gene_NS <- NULL
gene_S <- NULL
for (eff in unique(single_trans$effect)) {
  sym <- FALSE
  for (SL in Synonym){
    if (eff == SL)
      sym <- TRUE
  }    
  if (sym) {
    S_list <- c(S_list, single_trans$AF[single_trans$effect == eff])
    gene_S <- rbind(gene_S, data.frame(single_trans[single_trans$effect == eff, 
                                                      c("gene", "effect")]))

  }else{
    NS_list <- c(NS_list, single_trans$AF[single_trans$effect == eff]) 
    gene_NS <- rbind(gene_NS, data.frame(single_trans[single_trans$effect == eff, 
                                                    c("gene", "effect")]))
  }
}
gene_list_NS_eff <- table(droplevels(gene_NS))
gene_list_S_eff <- table(droplevels(gene_S))

gen_list_NS <- rowSums(gene_list_NS_eff)
gen_list_S <- rowSums(gene_list_S_eff)
```

Table 1 and Figure 1
Non Synonymous variations distribution per Chromosome

```{r NS_SNP-per-chromosome, echo=FALSE, results='asis'}
row.names(NSL_chr) <- NULL
pander(NSL_chr, caption = "Non Synonymous variation table")
barplot(NSL_chr$NS, names.arg = NSL_chr$chrm, 
        main = "Non_Synonymous variation distibution per Chromosome", col = rgb(0,0,1,1/4))
```

Table 2 and Figure 2
Synonymous variations distribution per Chromosome

```{r S_SNP-per-chromosome, echo=FALSE, results='asis' }
row.names(SL_chr) <- NULL
pander(SL_chr, caption = "Synonymous variation table", row.names = FALSE)
barplot(SL_chr$S, names.arg = SL_chr$chrm, 
        main = "Synonymous  variation distribution per Chromosome", col = rgb(1,0,0,1/4))
```

FIgure 3 and 4
Distribution per Gene

```{r SNP-per-gene, echo=FALSE, results='asis'}

hist(gen_list_NS, main = "Non_Synonymous variation distibution per Gene", 
     col = rgb(0,0,1,1/4), xlab = "SNP number per gene")
hist(gen_list_S, main = "Synonymous variation distibution per Gene", 
     col = rgb(1,0,0,1/4), xlab = "SNP number per gene")

```


## Several Varation affect Serine, Threonine and Tyrosine.

This amino acids perform on the phosphorylation.
We can see variations distribution who affect Serine, Threonine and Tyrosine without indiscriminate win or loose this amino acids (Table 3 and Figure 8).
We can see effect distribution and allele frequency distribution for each of them 
(table 4, 5 and 6 for effect distribution),
(figure 9 for allele frequency distribution)


Table 3 and Figure 8
Number of Variation change Serine, Threonine and Tyrosine

```{r NS-SNPs-change-S-T-Y, echo=FALSE, results='asis'}

data_S <- single_trans[grepl("[S]", single_trans$AA), ]
data_T <- single_trans[grepl("[T]", single_trans$AA), ]
data_Y <- single_trans[grepl("[Y]", single_trans$AA), ]
NS_S <- NULL
NS_T <- NULL
NS_Y <- NULL
for (Eff in NonSynonym){
  NS_S <- rbind(NS_S, data.frame(data_S[data_S$effect == Eff,]))
  NS_T <- rbind(NS_T, data.frame(data_T[data_T$effect == Eff,]))
  NS_Y <- rbind(NS_Y, data.frame(data_Y[data_Y$effect == Eff,]))
}
NSdata_S <- droplevels(NS_S)
NSdata_T <- droplevels(NS_T)
NSdata_Y <- droplevels(NS_Y)

hist_STY <- data.frame(AA_names = c("Serine", "Threonine", "Tyrosine" ),
                       Number = c(nrow(NSdata_S), nrow(NSdata_T), nrow(NSdata_Y)))
pander(hist_STY, caption = "Number of variation change STY Amino Acids")
barplot(hist_STY$Number, names.arg = hist_STY$AA_names, 
        main = "Number of variation change STY Amino Acids", col = rgb(0,1,0,1/4))
```

Table 4, 5 and 6
Effect distribution for ech Amino Acids (Serine, Threonine and Tyrosine)

```{r Effect-distribution-for-STY,echo=FALSE, results='asis'}
NSeff_S <- as.data.frame(table(NSdata_S$effect))
colnames(NSeff_S) <- c("Effect", "Freq")
NSeff_T <- as.data.frame(table(NSdata_T$effect))
colnames(NSeff_T) <- c("Effect", "Freq")
NSeff_Y <- as.data.frame(table(NSdata_Y$effect))
colnames(NSeff_Y) <- c("Effect", "Freq")
pander(NSeff_S, caption = "Variation Number by effect for Serine")
pander(NSeff_T, caption = "Variation Number by effect for Threonine")
pander(NSeff_Y, caption = "Variation Number by effect for Tyrosine")
```


```{r info}
sessionInfo()
```
