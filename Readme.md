Phosphorylation Network
============================================



*************************************************

Phosphorylation Network

This project involve to get every genetic and phosphoproteomic information in Yoruba people in Ibadan Nigeria

for that:

Determination of the genetic variation in phosphoproteomic

Change the amino acid in each protein sequence according to the genotype for each individuals

Observe the theorical modification in phosphorylation network


*************************************************

## First Step

selected SNPs in coding region

Use scripts in convertion folder


## Second Step

Selected SNPs within each phosphorylation domain for each protein 
  
Use scripts in convertion folder


## Third Step

change the amino acid in each protein for Non_Synonymous SNPs

Get SNP involved in Switches.ELM 

Use script in network folder

