#!/usr/bin/env python

'''
Convert table data file to fasta file 
'''

import sys, os

def get_args(cline = sys.argv[1:]):
    '''
    get input file command line and output file command line
    
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Convert table data file to fasta file''')
    
    parser.add_argument('input_file', help = 'file from R after get sequence info from BiomaRt')
    parser.add_argument('-o', '--output_file', help = 'output fasta file [default: stdout]',
                        metavar = "fname.fa")
    args = parser.parse_args(cline)

    return args
    
def fasta_convertion(Inp_file, Outp_file = sys.stdout):

    R_file = open(Inp_file, 'r')
    Fa_file = open(Outp_file, 'w')

    line = R_file.readline()
    
    while True:
        line = R_file.readline()
        if not line:
            break

        items = line.split('\t')
        info = '>sp|' + str.join('|',[items[3], items[2], items[1], items[4]])
        seq = items[0].replace('*', '')

        Fa_file.write(info + seq + '\n')

				
								
#####################################################################################################        
        
if __name__ == '__main__':
    args = get_args()
    if args.output_file:
        fasta_convertion(args.input_file,  args.output_file)
    else:
        fasta_convertion(args.input_file)
	