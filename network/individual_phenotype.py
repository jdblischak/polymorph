#!/usr/bin/env python

'''
Process to individual phenotype from list of SNPs
'''

import sys, os, re, shutil
from Bio import SeqIO

def get_args(cline = sys.argv[1:]):
    '''
    get input file command line and output file command line
    
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Convert table data file to fasta file''')
    
    parser.add_argument('input_file', help = 'SNP file with protein link')
    parser.add_argument('-p', '--prot_file', help = 'reference protein fasta file')
    parser.add_argument('-l', '--list_file', help = 'file of list of individual ID')
  
    args = parser.parse_args(cline)

    return args



def AA_modification(temp_file, gene_id, ind_file, aa_pos, aa_new):

    ind_temp = open(temp_file, 'w')
    for seq_record in SeqIO.parse(ind_file, "fasta"):
        if re.search(gene_id, seq_record.id):
            seq_list = list(seq_record.seq)
            if int(aa_pos)-1 in range(len(seq_list)):
                seq_list[int(aa_pos)-1] = aa_new
                ind_temp.write('>' + seq_record.id + '\n')
                ind_temp.write(''.join(seq_list) + '\n')
                continue
            
        ind_temp.write('>' + seq_record.id + '\n')
        ind_temp.write(str(seq_record.seq) + '\n')

    ind_temp.close()
    shutil.copy(temp_file, ind_file)
    
    
def individual_phenotype(snp_file, phospho_ref, individual_list):
    
    
    temp_folder =   os.path.join(os.path.dirname(phospho_ref), 'temp_ind_folder')
    if not os.path.exists(temp_folder):
        os.makedirs(temp_folder)

    ind_list = open(individual_list, 'r')
    ind_numb = 21
    while True:
        Iline = ind_list.readline()
        
        if not Iline:
            break
            
        #copy pretein reference for each indivual
        ind_id = Iline.split()[0]
        ind_file_name = ind_id + '.fa'
        ind_file = os.path.join(os.path.dirname(phospho_ref), ind_file_name)
        shutil.copy(phospho_ref, ind_file)
        
        temp_file_name =  ind_id + '_temp.fa'
        temp_file = os.path.join(temp_folder, temp_file_name)

        snp_list = open(snp_file, 'r')
        while True:
            Sline = snp_list.readline()
            if not Sline:
                break

            war = False
            if re.search('WARNING', Sline):
                war = True
                
            snp_info = Sline.split('\t')

            #check if need modofication
            if snp_info[12] != '':
                aa_info = re.split(r'(\d+)', snp_info[12])
                if aa_info[2] != '':
                    if war:
                        if re.search('1', snp_info[ind_numb+1]):
                            if snp_info[ind_numb+1] == '1|0' or snp_info[ind_numb+1] == '0|1':
                                if float(snp_info[6]) < 0.5:
                                    AA_modification(temp_file, snp_info[145], ind_file, aa_info[1], aa_info[2])
                            
                            else:
                                AA_modification(temp_file, snp_info[145], ind_file, aa_info[1], aa_info[2])
            
                    else:
                        if re.search('1', snp_info[ind_numb]):
                            if snp_info[ind_numb] == '1|0' or snp_info[ind_numb] == '0|1':
                                if float(snp_info[6]) < 0.5:
                                    AA_modification(temp_file, snp_info[144], ind_file, aa_info[1], aa_info[2])
                                    
                            else:
                                AA_modification(temp_file, snp_info[144], ind_file, aa_info[1], aa_info[2])
                        
        ind_numb += 1
        os.remove(temp_file)
    os.rmdir(temp_folder)
    

								
#####################################################################################################        
        
if __name__ == '__main__':
    args = get_args()
    individual_phenotype(args.input_file, args.prot_file, args.list_file)
   