#!/usr/bin/env python

'''
Get SNPs involved in switch domaine
'''

import sys, re

def get_args(cline = sys.argv[1:]):
    '''
    get input file command line and output file command line
    
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Get SNPs involved in switch.ELM database''')
    
    parser.add_argument('input_file', help = 'SNP file with protein link')
    parser.add_argument('-s', '--switch_file', help = 'switches.ELM dataset from http://switches.elm.eu.org')
    parser.add_argument('-o', '--output_file', help = 'file with SNPs cross with switches.ELM database [default: stdout]',
                        metavar = "fname.txt")
  
    args = parser.parse_args(cline)

    return args


def snp_switches(input_file, switch_file, cross_file = sys.stdout):
    

    snp_file = open(input_file, 'r')
    
    while True:
        Sline = snp_file.readline()
        if not Sline:
            snp_file.close()
            break
            
        snp_info = Sline.split('\t')
        switches = open(switch_file, 'rU')
        Wline = switches.readline()
        
        while True:
            Wline = switches.readline()
            if not Wline:
                switches.close()
                break
        
            if re.search(snp_info[-16], Wline):
                if snp_info[12] != '':
                    aa_pos = re.split(r'(\d+)', snp_info[12])[1]
                    switch_info = Wline.split('\t')
                    if re.search(snp_info[-16], switch_info[4]):
                        if not re.search(';', ''.join(switch_info[6:8])) and ''.join(switch_info[6:8]) != '':
                            if int(aa_pos) in range(int(switch_info[6]), int(switch_info[7])):
                                cross_file.write(Sline.split('\n')[0] + '\tSwitches involvement A:-->\t' + Wline)
                    elif re.search(snp_info[-16], switch_info[8]):
                        if not re.search(';', ''.join(switch_info[10:12])) and ''.join(switch_info[10:12]) != '':
                            if int(aa_pos) in range(int(switch_info[10]), int(switch_info[11])):
                                cross_file.write(Sline.split('\n')[0] + '\tSwitches involvement B:-->\t' + Wline)
                        
    cross_file.close()
    
#####################################################################################################        
        
if __name__ == '__main__':
    args = get_args()
    if args.output_file:
        cross_file = open(args.output_file, 'w')
        snp_switches(args.input_file, args.switch_file, cross_file)
    else:
        snp_switches(args.input_file, args.switch_file)