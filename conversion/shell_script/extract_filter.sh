#!/bin/bash


<<instruction

Run 'SnpSift.jar extractFields' Extract fields from a VCF file to a TXT, tab separated format
It's need 2 argument : Input folder with to process and output folder   

instruction

INPUT_DIR=$1
RESULT_DIR=$2

for filename in `ls $INPUT_DIR/*filter.vcf`
do
    echo "java -jar /home/y.fourne/tools/snpEff/SnpSift.jar extractFields $filename \
CHROM POS ID REF ALT NS AF GC 'EFF[*].EFFECT' 'EFF[*].IMPACT' 'EFF[*].FUNCLASS' 'EFF[*].CODON' \
'EFF[*].AA' 'EFF[*].AA_LEN' 'EFF[*].GENE' 'EFF[*].BIOTYPE' 'EFF[*].CODING' 'EFF[*].TRID' 'EFF[*].RANK' \
'GEN[*].GT' > $RESULT_DIR/`basename ${filename%vcf}`txt" | \
	qsub -l h_vmem=16g -N `basename $filename` -o $RESULT_DIR -j y -cwd -V
done
