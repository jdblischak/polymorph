#!/bin/bash


<<instruction

Run 'snpEff.jar' Annoteting vcf file java program on all vcf file in a folder
It's need 2 argument : Input folder with to process and output folder   

instruction

INPUT_DIR=$1
RESULT_DIR=$2

for filename in `ls $INPUT_DIR/*_haps.vcf`
do
    echo "java -Xmx6g -jar  /home/y.fourne/tools/snpEff/snpEff.jar hg19 -v $filename > $RESULT_DIR/`basename ${filename%vcf}`snpeff.vcf" | \
	qsub -l h_vmem=8g -N `basename $filename` -o $RESULT_DIR -j y -cwd -V
done
