#!/bin/bash


<<instruction 

Run convert_CDSfasta_to_vcf.py python program on all .fa file in a folder
It's need 2 argument : Input folder with to process and output folder for vcf output file  

instruction

IMPUTE_DIR=$1
RESULT_DIR=$2

for filename in `ls $IMPUTE_DIR/*.fa`
do
  echo "/mnt/lustre/home/y.fourne/polymorph/convert_CDSfasta_to_vcf.py $filename > $RESULT_DIR/`basename ${filename%fa}`vcf" | \
    qsub -l h_vmem=4g -N `basename $filename` -o $RESULT_DIR -j y -cwd -V
 
done
