
'''
Run `nosetests --verbosity=2` from base project directory.
'''

from convert_CDSfasta_to_vcf import get_args, add_header, convert_line, convert_CDSfasta_to_vcf

from nose.tools import assert_equal, assert_almost_equal, assert_true, \
    assert_false, assert_raises

from StringIO import StringIO

def test_convert_line():
   species_ref = 'hg19'
   species_comp =  ['panTro2', 'rheMac2']
   comparison =  { 'NM_032291_hg19_2_25 64 1 2 chr1:67091530-67091593+' : 'GATTGAAAAAACGTACAAGGAAGGCCTTTGGAATACGGAAGAAAGAAAAGGACACTGATTCTAC',
                   'NM_032291_panTro2_2_25 64 1 2 chr1:67803187-67803250+' : 'GACTGAAAAAACGTACAAGGAAGGCCTTTGGAATACGGAAGAAAGAAAAGGACACTGATTCTAC',
                   'NM_032291_rheMac2_2_25 64 1 2 chr1:69414133-69414196+' : 'GATTGAAAAAACGTACAAGGAAGGCCTTTGGAATACGGAAGAAAGAAAAGGACACTGATTCTAC'}
   result = convert_line(species_ref, species_comp, comparison)
   expected = 'chr1	67091532	chr1:67091532	T	C	PASS	.	GN=NM_032291;EN=2;TE=25;EL=64;IF=1;OF=2;VP=3	GT	1/1	0/0\n'
   assert_equal(result, expected)

def test_add_header():
    x = StringIO()
    add_header(x, 'hg19',['panTro2', 'rheMac2'])
    expected = '''\
##fileformat=VCFv4.2
##source=CDS fasta
##description=Alignment VCF file
##reference=hg19
##comparison=panTro2	rheMac2
##INFO=<ID=GN,Number=1,Type=String,Description="geneName-The name field from the genePred table"
##INFO=<ID=EN,Number=1,Type=Integer,Description="Exon Number-Exons are counted starting at one and begin at the transcription start site">
##INFO=<ID=TE,Number=1,Type=Integer,Description="Total Exons-The number of coding exons in the gene">
##INFO=<ID=EL,Number=1,Type=Integer,Description="Exon Length-The length of the current exons">
##INFO=<ID=IF,Number=1,Type=Integer,Description="InFrame-The frame number of the first nucleotide in the exon">
##INFO=<ID=OF,Number=1,Type=Integer,Description="outFrame-The frame number of the nucleotide after the last nucleotide in this exon">
##INFO=<ID=VP,Number=1,Type=Integer,Description="Variant position on Exon">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Species Genotype">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	panTro2	rheMac2
'''
    x.seek(0)
    assert_equal(x.read(), expected)

def test_convert_CDSfasta_to_vcf():
        result = StringIO()
        convert_CDSfasta_to_vcf(open('tests/data/refGene.exonNuc.fa', 'r'), 'hg19', ['panTro2', 'rheMac2'],  result)
        result.seek(0)
        expected = open('tests/data/refGene.exonNuc.vcf', 'r')
        assert_equal(result.read(), expected.read())
            