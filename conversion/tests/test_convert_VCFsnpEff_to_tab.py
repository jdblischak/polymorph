'''
Run `nosetests --verbosity=2` from base project directory.
'''

from convert_VCFsnpEff_to_tab import get_args, add_header, convert_line, convert_VCFsnpEff_to_tab

from nose.tools import assert_equal, assert_almost_equal, assert_true, \
    assert_false, assert_raises

from StringIO import StringIO

def test_convert_line():
    line = 'chr1    1577084 chr1:1577084:D  TTCCTCCTCC      T       0.0     .      \
    NS=60;AF=0.0833;GC=50,10,0;EFF=CODON_CHANGE_PLUS_CODON_DELETION(MODERATE||gaggaggaggaa/gaa|EEEE266E|735|CDK11B||CODING|NM_033488.1|10|1)        \
    GT      0|0     0|0     0|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|0     0|0     0|0     \
    0|0     0|0     0|0     0|0     0|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|0     1|0     0|0     0|0     1|0     0|0     \
    0|0     0|0     0|0     0|0     0|0		1|0		0|0     1|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     1|0     0|0     0|0     \
    0|0     0|1     0|0     0|0     0|0     0|0     0|0'
    result = convert_line(line)
    expected = 'chr1	1577084	chr1:1577084:D	TTCCTCCTCC	T	60	0.0833	50,10,0	CODON_CHANGE_PLUS_CODON_DELETION	MODERATE		gaggaggaggaa/gaa	EEEE266E	735	CDK11B		CODING	NM_033488.1	10	0|0	0|0	0|0	0|0	0|0	0|0	0|1	0|0	0|0	0|0	0|1	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|0	0|1	0|0	0|0	0|0	0|0	1|0	0|0	0|0	1|0	0|0	0|0	0|0	0|0	0|0	0|0	1|0	0|0	1|0	0|0	0|0	0|0	0|1	0|0	0|0	0|0	1|0	0|0	0|0	0|0	0|1	0|0	0|0	0|0	0|0	0|0'
    assert_equal(result, expected)

def test_add_header():
    x = StringIO()
    add_header(x, ['NS', 'AF', 'GC'])
    expected = '''#CHROM	POS	ID	REF	ALT	NS	AF	GC	EFF[*].EFFECT	EFF[*].IMPACT	EFF[*].FUNCLASS	EFF[*].CODON	EFF[*].AA	EFF[*].AA_LEN	EFF[*].GENE	EFF[*].BIOTYPE	EFF[*].CODING	EFF[*].TRID	EFF[*].RANK	GEN[*].GT\n'''
    x.seek(0)
    assert_equal(x.read(), expected)

def test_convert_VCFsnpEff_to_tab():
    result = StringIO()
    convert_VCFsnpEff_to_tab(open('tests/data/chr1.hg19.impute2_haps.filter.vcf', 'r'), result)
    result.seek(0)
    expected = open('tests/data/chr1.hg19.impute2_haps.filter.txt', 'r')
    assert_equal(result.read(), expected.read())
