'''
Run `nosetests --verbosity=2` from base project directory.
'''

from convert_impute2_to_vcf import get_args, add_header, convert_line, convert_impute2_to_vcf

from nose.tools import assert_equal, assert_almost_equal, assert_true, \
    assert_false, assert_raises

from StringIO import StringIO

def test_convert_line():
    line = '--- rs006 60006 T G 0 1 1 0 0 1 1 0 0 1'
    result = convert_line(line, 'chr20')
    expected = 'chr20	60006	rs006	T	G	PASS	.	NS=5;AF=0.5;GC=0,5,0	GT	0|1	1|0	0|1	1|0	0|1'
    assert_equal(result, expected)

def test_add_header():
    x = StringIO()
    add_header(x, 5)
    expected = '''##fileformat=VCFv4.2
##source=IMPUTE2
##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">
##INFO=<ID=AF,Number=A,Type=Float,Description="Allele Frequency">
##INFO=<ID=GC,Number=G,Type=Integer,Description="Genotype Counts">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	NA00001	NA00002	NA00003	NA00004	NA00005
'''
    x.seek(0)
    assert_equal(x.read(), expected)

def test_convert_impute2_to_vcf():
    result = StringIO()
    convert_impute2_to_vcf(open('tests/data/chr3.hg19.impute2_haps', 'r'), 'chr3', result)
    result.seek(0)
    expected = open('tests/data/chr3.hg19.impute2_haps.vcf', 'r')
    assert_equal(result.read(), expected.read())
