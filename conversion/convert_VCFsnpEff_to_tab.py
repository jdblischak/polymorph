#!/usr/bin/env python

'''
Convert output from VCF.snpEff after filter with SnpSift to tab file format.
VCF file format description: http://samtools.github.io/hts-specs/VCFv4.2.pdf
'''

import sys

def get_args(cline = sys.argv[1:]):
    '''
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Convert output from VCF.snEff to
                                     tab file format.''')
    parser.add_argument('input_file', help = 'VCF.snpEff output file from SnpSift')
    parser.add_argument('-o', '--output', help = 'output tab file [default: stdout]',
                        metavar = "fname.vcf")
    args = parser.parse_args(cline)
    return args

def add_header(fname, head_info):
    '''Writes columns names header to fname.

    Input
      fname: file handle
      num_samples: the number of samples with genotype information (integer)
    '''
    list_name = ["#CHROM", "POS", "ID", "REF", "ALT", "EFF[*].EFFECT", "EFF[*].IMPACT", "EFF[*].FUNCLASS", "EFF[*].CODON",\
        "EFF[*].AA", "EFF[*].AA_LEN", "EFF[*].GENE", "EFF[*].BIOTYPE", "EFF[*].CODING", "EFF[*].TRID", "EFF[*].RANK", "GEN[*].GT"]
    
    
    header = str.join('\t', [ str.join('\t', list_name[:5]), str.join('\t', head_info), str.join('\t', list_name[5:])]) + '\n'
    
    fname.write(header)
    
def convert_line(line):
    '''Convert line of VCF.snpEff to tab format.

    Input:
      line: a string of text
    
    Example:
      convert_line('chr1    1577084 chr1:1577084:D  TTCCTCCTCC      T       0.0     .      \
    NS=60;AF=0.0833;GC=50,10,0;EFF=CODON_CHANGE_PLUS_CODON_DELETION(MODERATE||gaggaggaggaa/gaa|EEEE266E|735|CDK11B||CODING|NM_033488.1|10|1)        \
    GT      0|0     0|0     0|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|0     0|0     0|0     \
    0|0     0|0     0|0     0|0     0|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|0     1|0     0|0     0|0     1|0     0|0     \
    0|0     0|0     0|0     0|0     0|0		1|0		0|0     1|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     1|0     0|0     0|0     \
    0|0     0|1     0|0     0|0     0|0     0|0     0|0')
      returns 'chr1    1577084 chr1:1577084:D  TTCCTCCTCC      T       60      0.0833  50,10,0 CODON_CHANGE_PLUS_CODON_DELETION        MODERATE\
    gaggaggaggaa/gaa        EEEE266E	735     CDK11B          CODING  NM_033488.1     10      0|0     0|0     0|0     0|0     0|0     0|0     \
    0|1     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|0	0|0     0|0     0|0     0|0     0|0     0|0     0|0     0|0     0|0     \
    0|0     0|1     0|0     0|0     0|0     0|0     1|0     0|0     0|0     1|0     0|0     0|0     0|0     0|0     0|0     0|0     1|0     \
    0|0     1|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     1|0     0|0     0|0     0|0     0|1     0|0     0|0     0|0     0|0     0|0'
    '''

    new_line = ""
    line_info = line.split()
    info = line_info[7].split(";")[:-1]
    snpeff = line_info[7].split(";")[-1]

    info_list = []
    for inf in info:
        info_list.append(inf.split("=")[-1])

    snpeff_list = [snpeff.split("(")[0].split("=")[-1]]
    snpeff_list = snpeff_list + snpeff.split("(")[-1].split("|")[:-1]
	snpeff_list.append(snpeff.split("(")[-1].split("|")[-1].split(')')[0])

    newline = str.join('\t', [ str.join('\t', line_info[:5]), str.join('\t', info_list), str.join('\t', snpeff_list),\
                           str.join('\t', line_info[9:])])
    
    return newline
    
def convert_VCFsnpEff_to_tab(input_file, tab_file = sys.stdout):
    '''Converts VCF.SnpEff file after filter with SnpSift to tab format.

    Input:
      input_file: file handle to read INPUT file
      tab_file: file handle to write tab output (default: sys.stdout)
    '''
    
    head_info = []
                      
    while True:
        line = input_file.readline()
        if not line[0] == '#':
            break
        if line.split('=')[0] == '##INFO' and not line.split("=")[2].split(',')[0] == 'EFF':
            head_info.append(line.split("=")[2].split(',')[0])

    add_header(tab_file, head_info)
    tab_file.write(convert_line(line) + '\n')
    
    while True:
        line = input_file.readline()
        if not line:
            break        
            
        tab_file.write(convert_line(line) + '\n')


    
#####################################################################################################
            
if __name__ == '__main__':
    args = get_args()
    file = open(args.input_file, 'r')
    if args.output:
        tab_file = open(args.tab_file, 'w')
        convert_VCFsnpEff_to_tab(file, tab_file)
    else:
        convert_VCFsnpEff_to_tab(file)
