Predicting Protein Effect
============================================

The objective is to predicting protein effect caused by genetics variations,
from different origin like Single Nucleotide Polymorphismin a population or 
sequences alignment between species.
 
-------------------------------------------------

The following tools have needed one of two type of input file:

+ Genotype input file: IMPUTE2

+ Alignment fasta file:CDS_Fasta (CoDing Sequence)

Ex : refGene.exonNuc.fa.gz from 46-vertebrate alignment of coding sequences from UCSC. 

Available here: http://hgdownload.soe.ucsc.edu/goldenPath/hg19/multiz46way/alignments/refGene.exonNuc.fa.gz

------------------------------------------------

####The First step is to convert the imput file to VCF file (Variant Call Format): 

IMPUTE2 : convert_impute2_to_vcf.py

CDS_Fasta : convert_CDSfasta_to_vcf.py

####The Second step is the use of SnpEff and SnpSift tools: Download and install SnpEff: http://snpeff.sourceforge.net

#### tools:
+ snpEff.jar: For annotation (specify species)
+ vcfEffOnePerLine.pl: For one effect per line(one variation can do some effect)
+ SnpSift.jar filter: Filter with any fields
+ SnpSift.jar extractFields: For convert to tab file (txt file) usable with other application(R,Exel,Open Office...)

For processing to many Input file in a directory, use the following Shell tools:
> N.B.:
Sometimes extractFields doesn't work.
If that's the case, use: convert_VCFsnpEff_to_tab.py (importante : one Effect per line)

-------------------------------------------------

#### Shell_script folder 
Shell scripts are used to process several file
Specify input and output folder in command line argument for all script 

run in order:

+ convert_from_IMPUTE2.sh or conver_from_CDSfasta: Run convert_impute2_to_vcf.py or convert_CDSfasta_to_vcf.py
+ annot.sh: Run snpEff.jar 
+ EffOnePerLine.sh: Run vcfEffOnePerLine.pl
+ filter.sh: Run SnpSift.jar filter
+ read next step before
+ extract_filter.sh: Run SnpSift.jar extractFields
>Default annotation: human species, hg19

----------------------------------------------------

#### After Filter:

You can join all VCF filter file in one file before extracting fields to tab file:

your end of output file name should be "filter.vcf"

Join all VCF filter file with this command line:

java -jar /home/y.fourne/tools/snpEff/SnpSift.jar split -j *.filter.vcf > ouput_file_name_filter.vcf

-------------------------------------------------------

After you run "extract_filter.sh", you get one txt file per VCF filter file and one txt file for your join file
